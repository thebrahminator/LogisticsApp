from app import db

class User(db.Model):
    id = db.Column(db.String(128), primary_key=True)
    name = db.Column(db.String(128))
    password = db.Column(db.String(128))
    email = db.Column(db.String(128))

    def __init__(self,id,name,password,email):
        self.id = id
        self.name = name
        self.password = password
        self.email = email

class Logistic(db.Model):
    id = db.Column(db.String(128), primary_key=True)
    name = db.Column(db.String(128))
    quantity = db.Column(db.Integer)

    def __init__(self,id,name,quantity):
        self.id = id
        self.name = name
        self.quantity = quantity

class Requirements(db.Model):
    id = db.Column(db.String(128), primary_key=True)
    username = db.Column(db.String(128))
    item = db.Column(db.String(128))
    quantity = db.Column(db.Integer)
    quantity_approved = db.Column(db.Integer)
    coreapp = db.Column(db.Integer)

    def __init__(self,id,username,item,quantity,quantity_approved,coreapp):
        self.id = id
        self.username = username
        self.item = item
        self.quantity = quantity
        self.quantity_approved = quantity_approved
        self.coreapp = coreapp
